from flask import jsonify, Blueprint, request
from flask_restx import Api, Resource, abort
from api.api.utils import refreshJwt, generateRefreshToken, verifyApiKey, generateAuthToken, checkAuthToken
from models import Post as BlogPost, User
from api import db, bcrypt

authorizations = {
    'apikey': {
        'type': 'apiKey',
        'bearerFormat': 'JWT',
        'in': 'header',
        'name': 'X-API-KEY'
    }
}

REST = Blueprint('api', __name__)
api = Api(REST, version = "1.0", 
    title = "Flutter blog mobile integration API",
    authorizations=authorizations
    )

from api.api.api_models import user_model, post, key
from api.api.parsers import post_parser, user_parser, refresh_token_parser

@api.route('/user/login/')
class Login(Resource):
    @api.response(200, 'User successfully logged in. returns JWT authentication token.')
    @api.response(404, 'user with email not found')
    @api.response(401, 'wrong password')
    @api.marshal_with(user_model, mask = "")
    @api.expect(user_parser)
    @api.doc(model='user')
    def post(self):
        args = user_parser.parse_args()
        user_query = User.query.filter_by(email = args["email"]).first()
        if not user_query:
            abort(404, exception = "email not found")
        user_query.refresh_token = generateRefreshToken()
        db.session.commit()
        if not bcrypt.check_password_hash(user_query.password, args["password"]):
            abort(401, exception = "wrong password")
        return {"id": user_query.id, "email": user_query.email, "refresh_token": user_query.refresh_token, "api_key": generateAuthToken(user_query)}

@api.route('/user/register/')
class Register(Resource):
    @api.response(409, 'Email already in use')
    @api.response(200, 'user created.')
    @api.marshal_with(user_model, mask = "")
    @api.expect(user_parser)
    @api.doc(model='user')
    def post(self):
        args = user_parser.parse_args()
        user = User.query.filter_by(email = args["email"]).first()
        if user:
            abort(409, exception = "email already in use")
        pw_hash = bcrypt.generate_password_hash(args["password"])
        refresh_token = generateRefreshToken()
        user = User(email = args["email"], password = pw_hash, refresh_token = refresh_token)   
        db.session.add(user)
        db.session.commit()
        return {"id": user.id, "email": user.email, "api_key": generateAuthToken(user), "refresh_token": refresh_token}

@api.route('/post/')
class Post(Resource):
    @api.doc(security='apikey')
    @api.expect(post_parser)
    @verifyApiKey
    def post(self):
        args = post_parser.parse_args()
        created_post = BlogPost(title = args["title"], body = args["body"], author = args["author"],  catagory = args["catagory"])
        db.session.add(created_post)
        db.session.commit()
        return {"success": "success"}

@api.route('/post/<int:post_num>/')
class PostByNum(Resource):
    @api.marshal_list_with(post, mask = "", envelope = "post")
    def get(self, post_num):
        created_post = BlogPost.query.filter_by(id = post_num).first()
        if not created_post:
            return {"exception" : "post not found"}, 404
        return Post.query.filter_by(id = post_num).first()

@api.route('/post/all/')
class Posts(Resource):
    @api.marshal_list_with(post, mask = "", envelope = "posts")
    def get(self):
        return BlogPost.query.all()

@api.route('/refresh_key/<int:user_id>/')
class Refresh(Resource):
    @api.expect(refresh_token_parser)
    @api.response(400, 'Invalid access token.')
    @api.response(422, 'Missing paramater')
    @api.response(401, 'wrong password')
    @api.response(200, 'User successfully refreshed. returns JWT authentication token.')
    def post(self, user_id):
        args = refresh_token_parser.parse_args()
        if "refresh token" not in args:
            return abort(422, exception = "missing paramater")
        user = User.query.filter_by(id = user_id).first()
        if not user:
            return abort(404, exception = "user not found")
        new_token = refreshJwt(args["refresh token"], user)
        if new_token:
            return {"token": new_token}
        else:
            return abort(401, exception = "bad refresh token")

@api.route('/test/')
class Test(Resource):
     @api.doc(security='apikey')
     @verifyApiKey
     def get(self):
        return "worked"