from flask import request
from api import app
from models import User
from functools import wraps
import jwt
import time
import random
import string

JWT_EXP_DELTA_SECONDS = 10 #60 * 60 * 24 * 15
JWT_SECRET = app.config["SECRET_KEY"]
JWT_ALGORITHM = "HS256"

#decorator
def verifyApiKey(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'X-API-KEY' in request.headers:
            id = checkAuthToken(request.headers['X-API-KEY'])
            if id:
                return f(*args, **kwargs)
            else:
                return {"error": "Bad api key"}, 401
        else:
            return {"error": "No api key"}, 400
        return f(*args, **kwargs)
    return decorated

def generateAuthToken(user):
    now = int(time.time())
    token = jwt.encode({"alg": JWT_ALGORITHM, "exp": now + JWT_EXP_DELTA_SECONDS, 
    "iat": now, "iss": "localhost:5000", "sub": user.email}, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return token.decode("utf-8")

def checkAuthToken(token, user = None):
    token = token.replace("Bearer ", "")
    try:
        decoded = jwt.decode(token, JWT_SECRET, issuer="localhost:5000", 
        algorithms=[JWT_ALGORITHM])
        if user:
            if user.email != decoded["sub"]: return 
        return decoded["sub"]
    except:
        return

def refreshJwt(refresh_token, user):
    identifier = User.query.filter_by(id = user.id).first()
    print("RT:" + refresh_token)
    print("IRT:" + identifier.refresh_token)
    if refresh_token == identifier.refresh_token:
        return generateAuthToken(user)
    else:
        return 

def generateRefreshToken():
    return ''.join([random.choice(string.ascii_letters + string.digits + '!@#$%^&*()_-~') for n in range(45)])
