from api.api.routes import api
from flask_restx import reqparse

user_parser = reqparse.RequestParser()
user_parser.add_argument('email', help="User's email", required=True, location = "form")
user_parser.add_argument('password', help="User's password (unhashed!)", required=True, location = "form")

refresh_token_parser = reqparse.RequestParser()
refresh_token_parser.add_argument('refresh token', help="Refresh token submitted to get new APIKey. Refresh token remains the same.", required=True, location = "form")

post_parser = reqparse.RequestParser()
post_parser.add_argument('title', help="post's title", required=True, location = "form")
post_parser.add_argument('author', help="author", required=True, location = "form")
post_parser.add_argument('body', help="body", required=True, location = "form")
post_parser.add_argument('catagory', help="catagory", required=True, location = "form")