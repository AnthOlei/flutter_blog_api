from api.api.routes import api
from flask_restx import Resource, fields
from api import app

user_model = api.model('user', {
    'id': fields.Integer(),
    'email': fields.String(),
    'refresh_token': fields.String(),
    'api_key': fields.String(),
})

api.models[user_model.name] = user_model

post = api.model('post', {
    'id': fields.Integer(),
    'title': fields.String(),
    'body': fields.String(),
    'date': fields.DateTime(),
    'author': fields.String(),
})

key = api.model('api key', {
    'key': fields.String()
})