from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from api.config import Config
from flask_bcrypt import Bcrypt

app = Flask(__name__)

bcrypt = Bcrypt()

app.config.from_object(Config)
db = SQLAlchemy(app)

db.init_app(app)
bcrypt.init_app(app)

from api.api.routes import REST 

app.register_blueprint(REST)